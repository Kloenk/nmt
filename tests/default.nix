{ nmt ? ./.., pkgs ? import <nixpkgs> { }, lib ? pkgs.lib }:

let
  modules.bash-lib = ({ config, ... }: {
    _module.args.pkgs = pkgs;
    nmt.preScript = ''
      out=$out/${config.nmt.name}
      mkdir -p $out

      echo "foo" > ./exists
    '';
    nmt.script = ''
      assertFileExists ./exists
      assertFileIsNotExecutable ./exists
      assertFileContent ./exists ./exists
    '';
  });

  tests = ((if nmt ? lib then nmt.lib.nmt else import nmt) {
    modules = [ ];
    inherit lib pkgs;
    tests.bash-lib = modules.bash-lib;
  });
in tests
