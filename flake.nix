{
  description =
    "This is a basic test framework for projects using the Nixpkgs module system. It is inspired by the nix-darwin test framework.";

  outputs = { self, nixpkgs }:
    let
      tests = import ./tests {
        nmt = self;
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
        lib = nixpkgs.lib;
      };
    in {

      lib.nmt = import ./.;

      checks.x86_64-linux = {
        test_all = tests.all;
        list = tests.list;
      } // nixpkgs.lib.mapAttrs' (name: config:
        nixpkgs.lib.nameValuePair name config.config.nmt.toplevel) tests.run;
    };
}
